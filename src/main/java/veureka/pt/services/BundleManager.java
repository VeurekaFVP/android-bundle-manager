package veureka.pt.services;

import android.content.Context;

public abstract class BundleManager {
    
    private static BundleManager managerInstance;
    static final String ERROR_MISSING_KEY_BUNDLE_MANAGER = "error_missing_key_bundle_manager";
    
    public static synchronized BundleManager getInstance() {
        if(managerInstance == null) {
            managerInstance = new BundleManagerImpl();
        }
        return managerInstance;
    }
    
    public abstract String displayMessage(Context context, String key, Object... messageArguments);
    
    public abstract String getMetadata(Context context, String key);
}
