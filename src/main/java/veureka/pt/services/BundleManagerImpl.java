package veureka.pt.services;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import java.text.MessageFormat;

public class BundleManagerImpl extends BundleManager {
    
    BundleManagerImpl() { }
    
    @Override
    public String displayMessage(Context context, String key, Object... messageArguments) {
        String packageName = context.getPackageName();
        String message;
        int id = 0;
        if(key != null) {
            id = context.getResources().getIdentifier(key, "string", packageName);
        }
        if(id == 0) {
            message = getErrorMessage(context, packageName, key);
        } else {
            message = getMessage(context.getString(id), messageArguments);
        }
        return message;
    }
    
    @Override
    public String getMetadata(Context context, String key) {
        String packageName = context.getPackageName();
        PackageManager packageManager = context.getPackageManager();
        String message;
        try {
            ApplicationInfo ai = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            message = bundle.getString(key);
            if(message == null) {
                message = getErrorMessage(context, packageName, key);
            }
        } catch(PackageManager.NameNotFoundException e) {
            message = getErrorMessage(context, packageName, key);
        }
        return message;
    }
    
    private String getMessage(String messageValue, Object... messageArguments) {
        String message;
        if(messageArguments != null && messageArguments.length > 0) {
            MessageFormat formatter = new MessageFormat(messageValue);
            message = formatter.format(messageArguments);
        } else {
            message = messageValue;
        }
        return message;
    }
    
    private String getErrorMessage(Context context, String packageName, String key) {
        int id = context.getResources().getIdentifier(ERROR_MISSING_KEY_BUNDLE_MANAGER, "string", packageName);
        String message = context.getString(id);
        return getMessage(message, key);
    }
}
