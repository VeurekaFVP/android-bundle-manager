package veureka.pt.services;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.Test;
import org.testng.annotations.BeforeClass;

import static org.junit.Assert.assertEquals;

public class BundleManagerTest {
    private static BundleManagerImpl manager = null;
    private static String TAG = "BundleManagrTest";

    @BeforeClass
    public static void initBundleManager(){
        Log.d(TAG,"iniciando el initBundleManager");
        Context appContext = InstrumentationRegistry.getTargetContext();
        manager = (BundleManagerImpl) BundleManager.getInstance();
        manager.addContext(appContext);
        Log.d(TAG,"fin del initBundleManager");
    }

    @Test
    public void getResourceStringTest(){
        if(manager == null){
            initBundleManager();
        }
        String appName = manager.displayMessage("app_name",null);
        assertEquals(appName,"BundleManager");
    }

    @Test
    public void getMissingResourceStringTest(){
        if(manager == null){
            initBundleManager();
        }
        String missingKey = manager.displayMessage("missing_key_testing",null);
        assertEquals(missingKey,"Error missing key");
    }

    @Test
    public void getMetadataStringTest(){
        if(manager == null){
            initBundleManager();
        }
        String appName = manager.getMetadata("veureka.pt.services\n.bundlemanager.name");
        assertEquals(appName,"bundleManagerName");
    }

    @Test
    public void getMissingMetadataStringTest(){
        if(manager == null){
            initBundleManager();
        }
        String missingKey = manager.getMetadata("veureka.pt.services.bundlemanager.missing.name");
        assertEquals(missingKey,"Error missing key");
    }

    @Test
    public void formatStringTest(){
        if(manager == null){
            initBundleManager();
        }
        Object[] params1 = {1,123,"3"};
        Object[] params2 = {false,"long string",null};
        Object[] params3 = {"3.14162021",3.14,123456};

        String formatString = manager.displayMessage("test_format_string",params1);
        assertEquals(formatString,"BundleManager 1 123 3");

        formatString = manager.displayMessage("test_format_string",params2);
        assertEquals(formatString,"BundleManager false long string null");

        formatString = manager.displayMessage("test_format_string",params3);
        assertEquals(formatString,"BundleManager 3.14162021 3,14 123.456");
    }
}
